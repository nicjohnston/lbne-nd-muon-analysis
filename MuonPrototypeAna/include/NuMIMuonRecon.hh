#ifndef __NUMIMUONRECON_HH__
#define __NUMIMUONRECON_HH__

class TH1;
#include <vector>
namespace MuonAna
{
  namespace NuMI
  {

    struct PulseInfo
    {
      double mean;
      double rms;
      double minVal;
      double maxVal;
      double start;
      int nbuckets;
      double total;
      std::vector<double> integ;
      std::vector<double> min;
      std::vector<double> max;
      double histIntegral;
    };
    void getMuonPulse(TH1* hist, PulseInfo& pulse);
    void AnalyzeMuonFiles(const char* file, const char* outfile);
    void getRwmPulse(TH1* hist, PulseInfo& pulse);
    void AnalyzeRwmFiles(const char* file, const char* outfile);
    void MergeMuonData(const char* output, const char* muonfile, const char* rwmfile, const char* acnetfile);

  }

}

#endif
