#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ namespace MuonAna;
#pragma link C++ namespace MuonAna::NuMI;
#pragma link C++ struct MuonAna::NuMI::PulseInfo;
#endif

