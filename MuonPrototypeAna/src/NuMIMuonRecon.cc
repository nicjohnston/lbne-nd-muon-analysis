#include "NuMIMuonRecon.hh"

#include "TH1.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "TMath.h"
#include "TGraph.h"
#include "TFile.h"
#include "TROOT.h"
#include "TChain.h"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>

// Added in order to time a loop
#include <time.h>

//#define DEBUG

using namespace std;
	//Bunches are 18.83 ns long
const double BucketWidth = 1./53;//0.01883;
const int nbuckets = 600;

void 
MuonAna::NuMI::getMuonPulse(TH1* hist, PulseInfo& pulse)
{
	pulse.mean = 0;
	pulse.rms = 0;
	pulse.minVal = 0;
	pulse.maxVal = 0;
	pulse.start = 0;
	pulse.total = 0;
	pulse.histIntegral = 0;
	//cout <<"Setting buckets"<<endl;
	if ( (int)pulse.integ.size()!= nbuckets)
		pulse.integ.resize(nbuckets);
	if ( (int)pulse.min.size()!= nbuckets)
		pulse.min.resize(nbuckets);
	if ( (int)pulse.max.size()!= nbuckets)
		pulse.max.resize(nbuckets);
//cout << "bf for loop" << endl;
	for (int i = 0 ; i < pulse.nbuckets; i++)
	{
		pulse.integ[i] = 0;
		pulse.min[i] = 1e9;
		pulse.max[i] = -1e9;
	}
//cout << "after for loop" << endl;
	pulse.start=-999;
	//cout << "+1" << endl;
	//char answer;
	//cout << "shall I spill?" << endl;
	//cin >> answer;
	int verbose = 0;
	int printinteg = 0;
	//if (answer == 'y') verbose=1;
	//if (answer == 'y') {
	//        //TCanvas *canvas = new TCanvas("c1", "testing");
	//        TFile f(Form("histo%d.root", j), "recreate");
	//        hist->Write();
	//        //app.Run();
	//        return;
	//}


	#ifdef DEBUG
		cout << "print integ? ";
		cin >> answer;
		if (answer == 'y') printinteg = 1;
		if (printinteg) {
			cout << "nbuckets = " << pulse.nbuckets << endl;
		}
	#endif


	int nbins = hist->GetNbinsX();
	//cout << "+2" << endl;
	int bin=0;
	pulse.minVal = hist->GetMinimum();
	//cout << "+4" << endl;
	pulse.maxVal = hist->GetMaximum();
	//cout <<"Getting mean"<<endl;
	for (int i = 1; i <= 500; i++)
	{
		double content=hist->GetBinContent(i);
		pulse.mean+=content;
		pulse.rms+=content*content;
	}
	//cout << "after get mean" << endl;
	pulse.mean /= 500;
	pulse.rms /= 500;
	pulse.rms = sqrt(pulse.rms - pulse.mean*pulse.mean);

	pulse.histIntegral = hist->Integral();

	if (pulse.histIntegral < 100) { // check for negative pulse first because it is more common (3 channels are negative, 1 is positive)
		double threshold = pulse.mean-5*pulse.rms;
		for (int i = 1; i<=nbins; i++) {
			if (hist->GetBinContent(i) < threshold && hist->GetBinContent(i+1) < threshold && hist->GetBinContent(i+2) < threshold){
				bin = i;
				break;
			}
		}

	} else { // assume a positive pulse
		double threshold = pulse.mean+5*pulse.rms;
		for (int i = 1; i<=nbins; i++) {
			if (hist->GetBinContent(i) > threshold && hist->GetBinContent(i+1) > threshold && hist->GetBinContent(i+2) > threshold){
				bin = i;
				break;
			}
		}
	}

	//cout << "halfway through finding pulse start" << endl;
	if (bin==0) return;
	int StartBin=1;
	for (int i = bin-1; i>=1;i--)
	{
		if (hist->GetBinContent(i)>pulse.mean) {
			StartBin=i;
			break;
		}
	}

	double StartTime = hist->GetXaxis()->GetBinLowEdge(StartBin);
	double BinWidth = hist->GetXaxis()->GetBinWidth(StartBin);
	double SpillWidth = pulse.nbuckets*BucketWidth;//overestimate to make sure we get all bunches
	double EndTime = StartTime + SpillWidth;
	int EndBin = hist->GetXaxis()->FindBin(EndTime);
	//cout <<"Pulse info"<<endl;
	for (int i = 0; i < nbuckets; i++)
	{
		if (verbose) {
			cout << "in pulse loop number " << i << endl;
		}
		double startTime = StartTime + i * BucketWidth;
		int startBin = hist->GetXaxis()->FindBin(startTime);
		double endTime = startTime + BucketWidth;
		int endBin = hist->GetXaxis()->FindBin(endTime);
		if (endBin > hist->GetNbinsX()) endBin = hist->GetNbinsX();
		if (startBin >= endBin) break;//We've hit the end of the histogram
		pulse.integ[i] = 0;
		//First bin
		pulse.integ[i] += (hist->GetBinContent(startBin)-pulse.mean) * (hist->GetXaxis()->GetBinUpEdge(startBin)-startTime)/BinWidth;
		//Last bin
		pulse.integ[i] += (hist->GetBinContent(endBin)-pulse.mean) * (endTime-hist->GetXaxis()->GetBinLowEdge(endBin))/BinWidth;
		//Other bins:
		for (int j = startBin+1; j<endBin; j++)
			pulse.integ[i] += hist->GetBinContent(j)-pulse.mean;

		for (int j = startBin; j<=endBin; j++){

			if (verbose) cout << "In 2nd for loop number " << j << endl;
				double val = hist->GetBinContent(j)-pulse.mean;
			if (verbose) cout << "In 2nd for loop number " << j << " after 1st line" << endl;
				if (val < pulse.min[i]) pulse.min[i] = val;
				if (val > pulse.max[i]) pulse.max[i] = val;

		}

		if (verbose) cout << "marker" << endl;
		if (endBin >EndBin) break;
	}
	pulse.start=StartTime;
	//cout <<"Adding up total"<<endl;
	for (int i = 0; i < pulse.nbuckets; i++)
	{
		pulse.total += pulse.integ[i];
	}

	if (printinteg) {
		cout << "nbuckets = " << pulse.nbuckets << endl;
		for (int i = 0; i < pulse.nbuckets; i++) {
			cout << "integ["<<i<<"] = " << pulse.integ[i] << endl;
		}
		cout << "total = " << pulse.total << endl;
	}
}


void
MuonAna::NuMI::getRwmPulse(TH1* hist, PulseInfo& pulse)
{
	getMuonPulse(hist,pulse);
	double nSamplesPerBucket = BucketWidth /hist->GetBinWidth(1);
	//Approximate baseline drift correction
	for (int i = 0; i < pulse.nbuckets; i++)
	{
		pulse.integ[i] -= nSamplesPerBucket * pulse.min[i];
		pulse.total -= nSamplesPerBucket * pulse.min[i];
	}

}

void MuonAna::NuMI::AnalyzeMuonFiles(const char* file, const char* outfile) {
	TChain * muon = new TChain("MuonData");
	muon->Add(file);
	TClonesArray* arr=0;
	ULong64_t time;
	int usec, nchan;
	int nchanOut = 0;

	muon->SetBranchAddress("time",&time);
	muon->SetBranchAddress("usec",&usec);
	muon->SetBranchAddress("data",&arr);
	muon->SetBranchAddress("nchan",&nchan);

	int n_ev = muon->GetEntries();

	// clock_t startCounter = clock();
	// for (int i = 0 ; i < n_ev; i++) {
	// 	muon->GetEntry(i);
	// }
	// printf( "%f\n", ( (double)clock() - startCounter ) / CLOCKS_PER_SEC );

	//this assumes the number of channels doesnt change in the input files
	muon->GetEntry(0);
	nchanOut = nchan - 1;
	// cout << nchan << endl;
	// cout << nchanOut << endl;
	// return;

	vector<PulseInfo> pulse(nchanOut);
	for (int i = 0; i < nchanOut; i++) pulse[i].nbuckets = nbuckets;
	double start[nchanOut];
	double integ[nchanOut][nbuckets];
	int startBin[nchanOut];
	double mean[nchanOut],rms[nchanOut], minVal[nchanOut],maxVal[nchanOut];
	double bmin[nchanOut][nbuckets], bmax[nchanOut][nbuckets];
	double total[nchanOut];
	double histIntegral[nchanOut];
	TFile fout(outfile,"RECREATE");
	fout.cd();
	TTree tr("MuonAna","Muon Analysis");

	tr.Branch("time",&time,"time/l");
	tr.Branch("usec",&usec,"usec/I");
	tr.Branch("start",start,TString::Format("start[%i]/D", nchanOut));
	tr.Branch("mean",mean,TString::Format("mean[%i]/D", nchanOut));
	tr.Branch("rms",rms,TString::Format("rms[%i]/D", nchanOut));
	tr.Branch("maxVal",maxVal,TString::Format("maxVal[%i]/D", nchanOut));
	tr.Branch("minVal",minVal,TString::Format("minVal[%i]/D", nchanOut));
	tr.Branch("startBin",startBin,TString::Format("startBin[%i]/I", nchanOut));
	tr.Branch("nbucket",&pulse[0].nbuckets,"nbucket/I");
	tr.Branch("integ",integ,TString::Format("integ[%i][%i]/D", nchanOut, nbuckets));
	tr.Branch("max",bmax,TString::Format("max[%i][%i]/D", nchanOut, nbuckets));
	tr.Branch("min",bmin,TString::Format("min[%i][%i]/D", nchanOut, nbuckets));
	tr.Branch("total",total,TString::Format("total[%i]/D", nchanOut));
	tr.Branch("histIntegral",histIntegral,TString::Format("histIntegral[%i]/D", nchanOut));

	gROOT->cd();
	for (int i = 0 ; i < n_ev; i++)
	{
		if (i%100==0) cout <<"Entry: "<<i<<" of "<<n_ev<<endl;
		muon->GetEntry(i);
		

		#ifdef DEBUG
			//TFile f(Form("majLoop%d.root", i), "recreate");
		#endif
		

		for (int j = 0; j < nchanOut; j++)
		{
			TH1* hist = (TH1*) arr->At(j+1);


			#ifdef DEBUG
				TFile f(Form("hist%d.root", j+1), "recreate");
						hist->Print();
					cout << "test line 0" << endl;
				hist->Write();
				char answer;
				cout << "shall I quit?" << endl;
				cin >> answer;
				if (answer == 'y') return;
				if (answer == 'r') int nbins = hist->GetNbinsX();
						cout << "calling getMuonPulse" << endl;
			#endif


			getMuonPulse(hist,pulse[j]);


			#ifdef DEBUG 
						cout << "test line 2" << endl;
			#endif


			startBin[j] = hist->GetXaxis()->FindBin(pulse[j].start);
			mean[j] = pulse[j].mean;
			rms[j] = pulse[j].rms;
			minVal[j] = pulse[j].minVal;
			maxVal[j] = pulse[j].maxVal;
			start[j] = pulse[j].start;
			total[j] = pulse[j].total;
			for (int k = 0; k < nbuckets; k++)
			{
				integ[j][k] = pulse[j].integ[k];
				bmin[j][k] = pulse[j].min[k];
				bmax[j][k] = pulse[j].max[k];
			}
			//cout <<"Integ[0]: "<<integ[j][0]<<endl;
			//cout <<"Total[0]: "<<total[j]<<endl;
			histIntegral[j] = pulse[j].histIntegral;
		}
		tr.Fill();
		
	}

	fout.cd();
	tr.Write();
	fout.Close();


}

void
MuonAna::NuMI::AnalyzeRwmFiles(const char* file, const char* outfile)
{
	TChain * rwm = new TChain("RWMData");
	rwm->Add(file);
	TClonesArray* arr=0;
	ULong64_t time;
	int usec;

	rwm->SetBranchAddress("time",&time);
	rwm->SetBranchAddress("usec",&usec);
	rwm->SetBranchAddress("data",&arr);
	int n_ev = rwm->GetEntries();

	PulseInfo pulse;
	pulse.nbuckets = nbuckets;
	double start;
	double integ[nbuckets];
	int startBin;
	double mean,rms, minVal,maxVal;
	double bmin[nbuckets], bmax[nbuckets];
	double total;
	TFile fout(outfile,"RECREATE");
	fout.cd();
	TTree tr("RwmAna","RWM Analysis");
	
	tr.Branch("time",&time,"time/l");
	tr.Branch("usec",&usec,"usec/I");
	tr.Branch("start",&start,"start/D");
	tr.Branch("mean",&mean,"mean/D");
	tr.Branch("rms",&rms,"rms/D");
	tr.Branch("maxVal",&maxVal,"maxVal/D");
	tr.Branch("minVal",&minVal,"minVal/D");
	tr.Branch("startBin",&startBin,"startBin/I");
	tr.Branch("nbucket",&pulse.nbuckets,"nbucket/I");
	tr.Branch("integ",integ,TString::Format("integ[%i]/D",nbuckets));
	tr.Branch("max",bmax,TString::Format("max[%i]/D",nbuckets));
	tr.Branch("min",bmin,TString::Format("min[%i]/D",nbuckets));
	tr.Branch("total",&total,"total/D");

	gROOT->cd();
	for (int i = 0 ; i < n_ev; i++)
	{
		if (i%100==0) cout <<"Entry: "<<i<<" of "<<n_ev<<endl;
		rwm->GetEntry(i);
		TH1* hist = (TH1*) arr->At(0);
		hist->Scale(-1);
		getRwmPulse(hist,pulse);
		startBin = hist->GetXaxis()->FindBin(pulse.start);
		mean = -pulse.mean;
		rms = pulse.rms;
		minVal = -pulse.maxVal;
		maxVal = -pulse.minVal;
		start = pulse.start;
		total = -pulse.total;
		for (int k = 0; k < nbuckets; k++)
		{
			integ[k] = -pulse.integ[k];
			bmin[k] = -pulse.max[k];
			bmax[k] = -pulse.min[k];
		}


		tr.Fill();
		
	}

	fout.cd();
	tr.Write();
	fout.Close();



}


namespace
{
	typedef struct
	{
		double timeD;
		ULong64_t time;
		int usec;
		double mean[5];
		double rms[5];
		double start[5];
		int startBin[5];
		int nb;
		double integ[5][nbuckets];
		double total[5];
		double bucketMin[5][nbuckets];
		double bucketMax[5][nbuckets];
		double min[5];
		double max[5];
	} MuonStruct;

	typedef struct
	{
		double timeD;
		ULong64_t time;
		int usec;
		double dT;
		double mean;
		double rms;
		double start;
		int startBin;
		int nb;
		double integ[nbuckets];
		double total[4];
		double bucketMin[nbuckets];
		double bucketMax[nbuckets];
		double min;
		double max;
	} RwmStruct;


	typedef struct
	{
		
		double pot;
		double dt_pot;
		double pot2;
		double dt_pot2;
		double T1;
		double T2;
		double pitch;
		double yaw;
		double Palc;
		double Ppump;
		double Pmks;
		double Par;
		double dt_mu;
		double lineA;
		double lineB;
		double lineC;
		double lineD;
		double tttgth;
		double hornI;
		double dt_horn;
		double mm1;
		double mm2;
		double mm3;
		double mm1x;
		double mm1y;
		double mm2x;
		double mm2y;
		double mm1cor;
		double mm2cor;
		double mm3cor;
		double mm1gas;
		double mm2gas;
		double mm3gas;
		double dt_mm;
		double dt_mmAltTiming;
		double gasMon2;
		double tgtx;
		double tgty;
		double dt_bpm;

		// Arrays
		double dt_mma1;
		double mma1ds[81];
		double mma1pd[81];
		double mma1cor[81];
		double mma2ds[81];
		double mma2pd[81];
		double mma2cor[81];
	} AcnetStruct;

	void loadEntry(double t,TTree* tree,double& entryT);

}
//#define DEBUG
void
MuonAna::NuMI::MergeMuonData(const char* output, const char* muonfile, const char* rwmfile, const char* acnetfile)
{
	//Set up output histo

	TChain mu_ch("MuonAna");
	mu_ch.Add(muonfile);
	TChain rwm_ch("RwmAna");
	rwm_ch.Add(rwmfile);
	TChain pot2_ch("POT");
	TChain pot_ch("POT2");
	TChain mm_ch("NuMIMu");
	TChain mmAltTiming_ch("NuMIMuAltTiming");
	TChain horn_ch("Horn");
	TChain muac_ch("Muon");
	TChain bpm_ch("BPM");
	TChain mmArray_ch("NuMIMuArray");

	pot_ch.Add(acnetfile);
	pot2_ch.Add(acnetfile);
	muac_ch.Add(acnetfile);
	mm_ch.Add(acnetfile);
	mmAltTiming_ch.Add(acnetfile);
	horn_ch.Add(acnetfile);
	bpm_ch.Add(acnetfile);
	mmArray_ch.Add(acnetfile);

	static ::MuonStruct muon;
	static ::RwmStruct rwm;
	static ::AcnetStruct acnet;

	//Set branches
	mu_ch.SetBranchAddress("time",&muon.time);
	mu_ch.SetBranchAddress("usec",&muon.usec);
	mu_ch.SetBranchAddress("mean",muon.mean);
	mu_ch.SetBranchAddress("rms",muon.rms);
	mu_ch.SetBranchAddress("start",muon.start);
	mu_ch.SetBranchAddress("startBin",muon.startBin);
	mu_ch.SetBranchAddress("integ",muon.integ);
	mu_ch.SetBranchAddress("total",muon.total);
	mu_ch.SetBranchAddress("min",muon.bucketMin);
	mu_ch.SetBranchAddress("max",muon.bucketMax);
	mu_ch.SetBranchAddress("minVal",muon.min);
	mu_ch.SetBranchAddress("maxVal",muon.max);
	mu_ch.SetBranchAddress("nbucket",&muon.nb);

	rwm_ch.SetBranchAddress("time",&rwm.time);
	rwm_ch.SetBranchAddress("usec",&rwm.usec);
	rwm_ch.SetBranchAddress("mean",&rwm.mean);
	rwm_ch.SetBranchAddress("rms",&rwm.rms);
	rwm_ch.SetBranchAddress("start",&rwm.start);
	rwm_ch.SetBranchAddress("startBin",&rwm.startBin);
	rwm_ch.SetBranchAddress("integ",rwm.integ);
	rwm_ch.SetBranchAddress("total",&rwm.total);
	rwm_ch.SetBranchAddress("min",rwm.bucketMin);
	rwm_ch.SetBranchAddress("max",rwm.bucketMax);
	rwm_ch.SetBranchAddress("minVal",&rwm.min);
	rwm_ch.SetBranchAddress("maxVal",&rwm.max);
	rwm_ch.SetBranchAddress("nbucket",&rwm.nb);

	pot_ch.SetBranchAddress("POT",&acnet.pot);
	pot2_ch.SetBranchAddress("POT",&acnet.pot2);
	muac_ch.SetBranchAddress("T1",&acnet.T1);
	muac_ch.SetBranchAddress("T2",&acnet.T2);
	muac_ch.SetBranchAddress("pitch",&acnet.pitch);
	muac_ch.SetBranchAddress("yaw",&acnet.yaw);
	muac_ch.SetBranchAddress("PAlcove",&acnet.Palc);
	muac_ch.SetBranchAddress("PPump",&acnet.Ppump);
	muac_ch.SetBranchAddress("PMKS",&acnet.Pmks);
	muac_ch.SetBranchAddress("PAr",&acnet.Par);
	horn_ch.SetBranchAddress("lineA",&acnet.lineA);
	horn_ch.SetBranchAddress("lineB",&acnet.lineB);
	horn_ch.SetBranchAddress("lineC",&acnet.lineC);
	horn_ch.SetBranchAddress("lineD",&acnet.lineD);
	// horn_ch.SetBranchAddress("tttgth",&acnet.tttgth);
	mm_ch.SetBranchAddress("mm1",&acnet.mm1);
	mm_ch.SetBranchAddress("mm2",&acnet.mm2);
	mm_ch.SetBranchAddress("mm3",&acnet.mm3);
	mm_ch.SetBranchAddress("mm1x",&acnet.mm1x);
	mm_ch.SetBranchAddress("mm1y",&acnet.mm1y);
	mm_ch.SetBranchAddress("mm2x",&acnet.mm2x);
	mm_ch.SetBranchAddress("mm2y",&acnet.mm2y);
	mm_ch.SetBranchAddress("mm1cor",&acnet.mm1cor);
	mm_ch.SetBranchAddress("mm2cor",&acnet.mm2cor);
	mm_ch.SetBranchAddress("mm3cor",&acnet.mm3cor);
	// mm_ch.SetBranchAddress("mm1gas",&acnet.mm1gas);
	// mm_ch.SetBranchAddress("mm2gas",&acnet.mm2gas);
	// mm_ch.SetBranchAddress("mm3gas",&acnet.mm3gas);
	bpm_ch.SetBranchAddress("gasMon2",&acnet.gasMon2);
	bpm_ch.SetBranchAddress("tgtX",&acnet.tgtx);
	bpm_ch.SetBranchAddress("tgtY",&acnet.tgty);

	mmAltTiming_ch.SetBranchAddress("mm1gas",&acnet.mm1gas);
	mmAltTiming_ch.SetBranchAddress("mm2gas",&acnet.mm2gas);
	mmAltTiming_ch.SetBranchAddress("mm3gas",&acnet.mm3gas);
	mmAltTiming_ch.SetBranchAddress("tttgth",&acnet.tttgth);

	//Arrays
	mmArray_ch.SetBranchAddress("mma1ds", acnet.mma1ds);
	mmArray_ch.SetBranchAddress("mma1pd", acnet.mma1pd);
	mmArray_ch.SetBranchAddress("mma2ds", acnet.mma2ds);
	mmArray_ch.SetBranchAddress("mma2pd", acnet.mma2pd);

	mu_ch.SetEstimate(mu_ch.GetEntries()+1);
	rwm_ch.SetEstimate(rwm_ch.GetEntries()+1);
	pot_ch.SetEstimate(pot_ch.GetEntries()+1);
	pot2_ch.SetEstimate(pot2_ch.GetEntries()+1);
	muac_ch.SetEstimate(muac_ch.GetEntries()+1);
	mm_ch.SetEstimate(mm_ch.GetEntries()+1);
	horn_ch.SetEstimate(horn_ch.GetEntries()+1);
	bpm_ch.SetEstimate(horn_ch.GetEntries()+1);  // Does it make sense to use the horn channel's entries" - Nic on 01-17-19
	mmArray_ch.SetEstimate(mmArray_ch.GetEntries()+1);

	mu_ch.Draw("time+1e-6*usec","","goff");
	rwm_ch.Draw("time+1e-6*usec","","goff");
	pot_ch.Draw("time","","goff");
	pot2_ch.Draw("time","","goff");
	muac_ch.Draw("time","","goff");
	mm_ch.Draw("time","","goff");
	mmAltTiming_ch.Draw("time","","goff");
	horn_ch.Draw("time","","goff");
	bpm_ch.Draw("time","","goff");//printf("Reached line 552\n");
	mmArray_ch.Draw("time","","goff");//printf("Reached line 553\n");

/*
	ULong64_t nmu = mu_ch.Draw("time+1e-6*usec","","goff");
	ULong64_t nrwm = rwm_ch.Draw("time+1e-6*usec","","goff");
	ULong64_t npot = pot_ch.Draw("time","","goff");
	ULong64_t npos = pos_ch.Draw("time","","goff");
	ULong64_t np1 = p1_ch.Draw("time","","goff");
	ULong64_t np2 = p2_ch.Draw("time","","goff");
	ULong64_t nt = t_ch.Draw("time","","goff");
*/

	TFile outf(output,"RECREATE");
	outf.cd();
	TTree out("MuMonData","Muon Monitor Aggregated Data");
	
	out.Branch("timeD",&muon.timeD,"timeD/D");
	out.Branch("time",&muon.time,"time/l");
	out.Branch("usec",&muon.usec,"usec/I");
	out.Branch("mean",muon.mean,"mean[5]/D");
	out.Branch("rms",muon.rms,"rms[5]/D");
	out.Branch("start",muon.start,"start[5]/D");
	out.Branch("nbm",muon.nb,"nbm/I");
	out.Branch("startBin",muon.startBin,"startBin[5]/I");
	out.Branch("integ",muon.integ,TString::Format("integ[5][%i]/D",nbuckets));
	out.Branch("bucketMin",muon.bucketMin,TString::Format("bucketMin[5][%i]/D",nbuckets));
	out.Branch("bucketMax",muon.bucketMax,TString::Format("bucketMax[5][%i]/D",nbuckets));
	out.Branch("total",muon.total,"total[5]/D");
	out.Branch("min",muon.min,"min[5]/D");
	out.Branch("max",muon.max,"max[5]/D");
	out.Branch("rtimeD",&rwm.timeD,"rtimeD/D");
	out.Branch("rtime",&rwm.time,"rtime/l");
	out.Branch("rusec",&rwm.usec,"rusec/I");
	out.Branch("rmean",&rwm.mean,"rmean/D");
	out.Branch("rrms",&rwm.rms,"rrms/D");
	out.Branch("rstart",&rwm.start,"rstart/D");
	out.Branch("rstartBin",&rwm.startBin,"rstartBin/I");
	out.Branch("nbr",rwm.nb,"nbr/I");
	out.Branch("rinteg",rwm.integ,TString::Format("rinteg[%i]/D",nbuckets));
	out.Branch("rbucketMin",rwm.bucketMin,TString::Format("rbucketMin[%i]/D",nbuckets));
	out.Branch("rbucketMax",rwm.bucketMax,TString::Format("rbucketMax[%i]/D",nbuckets));
	out.Branch("rtotal",rwm.total,"rtotal/D");
	out.Branch("rmin",&rwm.min,"rmin/D");
	out.Branch("rmax",&rwm.max,"rmax/D");
	//out.Branch("acnet",&acnet,"pot/D:dt_pot:T1:T2:dt_T:pitch:yaw:dt_angle:Palc:dt_p1:Ppump:Pmks:Par:dt_p2");
	out.Branch("pot",&acnet.pot,"pot/D");
	out.Branch("dt_pot",&acnet.dt_pot,"dt_pot/D");
	out.Branch("pot2",&acnet.pot2,"pot2/D");
	out.Branch("dt_pot2",&acnet.dt_pot2,"dt_pot2/D");
	out.Branch("T1",&acnet.T1,"T1/D");
	out.Branch("T2",&acnet.T2,"T2/D");
	out.Branch("pitch",&acnet.pitch,"pitch/D");
	out.Branch("yaw",&acnet.yaw,"yaw/D");
	out.Branch("Palc",&acnet.Palc,"Palc/D");
	out.Branch("Ppump",&acnet.Ppump,"Ppump/D");
	out.Branch("Pmks",&acnet.Pmks,"Pmks/D");
	out.Branch("Par",&acnet.Par,"Par/D");
	out.Branch("dt_mu",&acnet.dt_mu,"dt_mu/D");
	out.Branch("lineA",&acnet.lineA,"lineA/D");
	out.Branch("lineB",&acnet.lineB,"lineB/D");
	out.Branch("lineC",&acnet.lineC,"lineC/D");
	out.Branch("lineD",&acnet.lineD,"lineD/D");
	out.Branch("hornI",&acnet.hornI,"hornI/D");
	out.Branch("tttgth",&acnet.tttgth,"tttgth/D");
	out.Branch("dt_horn",&acnet.dt_horn,"dt_horn/D");
	out.Branch("mm1",&acnet.mm1,"mm1/D");
	out.Branch("mm2",&acnet.mm2,"mm2/D");
	out.Branch("mm3",&acnet.mm3,"mm3/D");
	out.Branch("mm1x",&acnet.mm1x,"mm1x/D");
	out.Branch("mm1y",&acnet.mm1y,"mm1y/D");
	out.Branch("mm2x",&acnet.mm2x,"mm2x/D");
	out.Branch("mm2y",&acnet.mm2y,"mm2y/D");
	out.Branch("mm1cor",&acnet.mm1cor,"mm1cor/D");
	out.Branch("mm2cor",&acnet.mm2cor,"mm2cor/D");
	out.Branch("mm3cor",&acnet.mm3cor,"mm1cor/D");
	out.Branch("mm1gas",&acnet.mm1gas,"mm1gas/D");
	out.Branch("mm2gas",&acnet.mm2gas,"mm2gas/D");
	out.Branch("mm3gas",&acnet.mm3gas,"mm1gas/D");
	out.Branch("dt_mm",&acnet.dt_mm,"dt_mm/D");
	out.Branch("dt_mmAltTiming",&acnet.dt_mmAltTiming,"dt_mmAltTiming/D");
	out.Branch("gasMon2",&acnet.gasMon2,"gasMon2/D");
	out.Branch("tgtX",&acnet.tgtx,"tgtX/D");
	out.Branch("tgtY",&acnet.tgty,"tgtY/D");
	out.Branch("dt_bpm",&acnet.dt_bpm,"dt_bpm/D");    //printf("Reached line 634\n");

	//Arrays
	out.Branch("mma1ds",acnet.mma1ds,"mma1ds[81]/D");
	out.Branch("mma1pd",acnet.mma1pd,"mma1pd[81]/D");
	out.Branch("mma1cor",acnet.mma1cor,"mma1cor[81]/D");
	out.Branch("mma2ds",acnet.mma2ds,"mma2ds[81]/D");
	out.Branch("mma2pd",acnet.mma2pd,"mma2pd[81]/D");
	out.Branch("mma2cor",acnet.mma2cor,"mma2cor[81]/D");
	out.Branch("dt_mma1",&acnet.dt_mma1,"dt_mma1/D");


	gROOT->cd();    //printf("Reached line 643\n");
#ifdef DEBUG
int sc = 0;
#endif
	for (int i = 0; i< mu_ch.GetEntries(); i++)
	{
#ifdef DEBUG
int mc = 0;
#endif
		if (i%1000==0) cout <<"Entry : "<<i <<" of "<<mu_ch.GetEntries()<<endl;
		mu_ch.GetEntry(i);
		if (i%1000==0) { 
			cout <<"\tMeans: "<<muon.mean[0]<<" "<<muon.mean[1]<<" "<<muon.mean[2]<<" "<<muon.mean[3]<<" "<<muon.mean[4]<<endl;
			cout <<"\tSizeof(integ) = "<<sizeof(muon.integ)/sizeof(double)<<endl;
			cout <<"\tInteg[][10]: "<<muon.integ[0][10]<<" "<<muon.integ[1][10]<<" "<<muon.integ[2][10]<<" "<< muon.integ[3][10]<<" "<< muon.integ[4][10]<<" "<< muon.integ[5][10]<<endl;
		}
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		muon.timeD = muon.time + 1e-6 * muon.usec;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&rwm_ch,rwm.timeD);
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&pot_ch,acnet.dt_pot);
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&pot2_ch,acnet.dt_pot2);
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&muac_ch,acnet.dt_mu);
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&horn_ch,acnet.dt_horn);
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&mm_ch,acnet.dt_mm);
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&bpm_ch,acnet.dt_mm);            //printf("Reached line 662\n");
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&mmArray_ch,acnet.dt_mma1);      //printf("Reached line 663\n");
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		loadEntry(muon.timeD,&mmAltTiming_ch,acnet.dt_mmAltTiming);
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.dt_pot -= muon.timeD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.dt_pot2 -= muon.timeD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.dt_mu -= muon.timeD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.dt_horn -= muon.timeD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.dt_mm -= muon.timeD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.dt_mmAltTiming -= muon.timeD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.dt_mma1 -= muon.timeD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		acnet.hornI = acnet.lineA+acnet.lineB+acnet.lineC+acnet.lineD;
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		for (int i = 0; i < 81; i++) {
			acnet.mma1cor[i] = acnet.mma1ds[i] - acnet.mma1pd[i];
			acnet.mma2cor[i] = acnet.mma2ds[i] - acnet.mma2pd[i];
		}
#ifdef DEBUG
printf("test sc=%d\tmc=%d\n",sc,mc); mc++;
#endif
		out.Fill();
#ifdef DEBUG
sc++;
#endif
	}

	outf.cd();
	out.Write();
	outf.Close();

}


namespace{
	void  loadEntry(double t, TTree* tree, double& entryT)
{

	//Binary search for time
	Long64_t N = tree->GetEntries();
	Long64_t pos = TMath::BinarySearch(tree->GetEntries(),tree->GetV1(),t);
	
	if (pos==-1){
		tree->GetEntry(0);
		entryT = tree->GetV1()[0];
	}else if (pos==N-1)  {
		tree->GetEntry(tree->GetEntries()-1);
		entryT = tree->GetV1()[tree->GetEntries()-1];
	}else{
		double t1 = fabs(tree->GetV1()[pos]-t);
		double t2 = fabs(tree->GetV1()[pos+1]-t);
		if (t1 < t2){
			tree->GetEntry(pos);
			entryT = tree->GetV1()[pos];
		}else{
			tree->GetEntry(pos+1);
			entryT = tree->GetV1()[pos+1];
		}
	}


}

}
