#include "NuMIMuonRecon.hh"
//#include "TApplication.h"
//#include "TCanvas.h"
#include <iostream>
int main(int argc, const char* argv[])
{
   
  if (argc<3) {
    std::cerr<<"USAGE ERROR: RunMuonAna INPUT OUTPUT"<<std::endl;
    return -1;
  }
//TApplication app("app", nullptr, nullptr);
//TCanvas *canvas = new TCanvas("c1", "testing");
  const char* muonData = argv[1];
  const char* output = argv[2];

  MuonAna::NuMI::AnalyzeMuonFiles(muonData,output);
  //app.Run();
  return 0;
}

