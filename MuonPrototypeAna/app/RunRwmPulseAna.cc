#include "NuMIMuonRecon.hh"
#include <iostream>
int main(int argc, const char* argv[])
{
   
  if (argc<3) {
    std::cerr<<"USAGE ERROR: RunRwmAna INPUT OUTPUT"<<std::endl;
    return -1;
  }

  const char* muonData = argv[1];
  const char* output = argv[2];

  MuonAna::NuMI::AnalyzeRwmFiles(muonData,output);
  return 0;
}

