#include "../include/NuMIMuonRecon.hh"
#include <iostream>

int main(int argc, const char* argv[])
{
  if (argc<5)
  {
    std::cerr << "USAGE ERROR: MergeMuonResults output muon rwm acnet"<<std::endl;
 
    return -1;
  }

  const char* output = argv[1];
  const char* muon = argv[2];
  const char* rwm = argv[3];
  const char* acnet = argv[4];

  MuonAna::NuMI::MergeMuonData(output,muon,rwm,acnet);

  return 0; 
}
