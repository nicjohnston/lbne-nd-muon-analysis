/** File containing code to read ACNET output files in the form generated by Geoff Mills in July 2015. The file contains:
POT info, temperature info, Cherenkov counter pitch & yaw, and 4 pressure readings. The times do not all match, so the data is split into five separate ROOT trees.

Compile with:

$> g++ ReadAcnetFile.cc $(root-config --libs) $(root-config --cflags) -o ReadAcnetFile

To run:

$> ReadAcnetFile $INPUT $OUTPUT

If you want to combine the output from multiple input files, just use the ROOT hadd tool to merge the trees.

Author: Jeremy Lopez
Date: July 2015
*/

#include "TFile.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TTree.h"
#include "TH1.h"
#include "TMath.h"
#include <cstdio>
#include <cstring>
#include <cassert>
#include <string>
#include <sstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "zlib.h"
using namespace std;

/* Top part taken from Zlib documentation on how to read a compressed file

*/

#define CHUNK 16384
/* Decompress from file source to file dest until stream ends or EOF.
   inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_DATA_ERROR if the deflate data is
   invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
   the version of the library linked do not match, or Z_ERRNO if there
   is an error reading or writing the files. */
int inf(FILE *source, string& dest)
{
    dest = "";
    int ret;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit2(&strm,MAX_WBITS | 16 );
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)inflateEnd(&strm);
            return Z_ERRNO;
        }
        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                return ret;
            }
            have = CHUNK - strm.avail_out;
            string temp(out,out+have);
            dest.append(temp);
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

/* report a zlib or i/o error */
void zerr(int ret)
{
    fputs("zpipe: ", stderr);
    switch (ret) {
    case Z_ERRNO:
        if (ferror(stdin))
            fputs("error reading stdin\n", stderr);
        if (ferror(stdout))
            fputs("error writing stdout\n", stderr);
        break;
    case Z_STREAM_ERROR:
        fputs("invalid compression level\n", stderr);
        break;
    case Z_DATA_ERROR:
        fputs("invalid or incomplete deflate data\n", stderr);
        break;
    case Z_MEM_ERROR:
        fputs("out of memory\n", stderr);
        break;
    case Z_VERSION_ERROR:
        fputs("zlib version mismatch!\n", stderr);
    }
}


/**
Reads file header & extracts initial timestamp from which the row times are counted.
Throws out a bunch of text files that don't have useful info to put in the trees.

*/

void get_header(istringstream& iss,time_t& t0)
{
  string line;
//  getline(iss,line);
  string temp;
  //cout <<"Line: "<<line<<endl;
  unsigned long l;
  iss >> temp >> l;
  //cout <<"Temp: "<<temp<<" L "<<l<<endl;
  t0 = l / 1000;
  for (int i  = 0; i<3; i++){
    getline(iss,line);
   // cout <<"Dumping line: "<<line<<endl;
  }
 // cout <<"Start time "<<t0<<endl;
}
/**
Opens the output file and generates a set of ROOT trees from the file contents. Saves trees into file and then closes the file.
*/
void make_trees(istringstream& iss, time_t& t0, const char* filename)
{
  TFile f(filename,"RECREATE");
  f.cd();
  //Set up trees
  
  /*
  E:TORTGT : POT (x10^12)
  E:MA2T01 : Cherenkov Temp A C
  E:MA2T01 : Cherenkov Temp B C
  E:MA2X01 : Pitch %
  E:MA2X02 : Yaw %
  E:MA2P01 : QB2 pressure, mu alcove, psia
  E:MA2P01 : Vacuum pump inlet pres, psia
  E:MA2P03 : MKS gauge Ch 2, torr
  E:MA2PAR : Argon pressure at rack, psig
  E:TRTTGT
  Others not included:
    Horn currents
    Diamond voltage
    CD PMT voltage
  */

  //POT
  TTree potTree("POT","ACNET POT Info");
  double potTime;
  double pot;
  bool hasPOT;
  potTree.Branch("time",&potTime,"time/D");
  potTree.Branch("POT",&pot,"POT/D");
//  potTree.Branch("hasData",&hasPOT,"hasData/O");
  //Temperature  
  TTree muTree("Muon","ACNET Muon Prototype Info");
  double muTime;
  double temp1,temp2;
  bool hasMu;
  muTree.Branch("time",&muTime,"time/D");
  muTree.Branch("T1",&temp1,"T1/D");
  muTree.Branch("T2",&temp2,"T2/D");
  double pitch,yaw;
  muTree.Branch("pitch",&pitch,"pitch/D");
  muTree.Branch("yaw",&yaw,"yaw/D");
  double pres1,pres2;
  muTree.Branch("PAlcove",&pres1,"PAlcove/D");
  double p;//
  double pAr;
  muTree.Branch("PPump",&pres2,"PPump/D");
  muTree.Branch("PMKS",&p,"PMKS/D");
  muTree.Branch("PAr",&pAr,"PAr/D");

  TTree potTree2("POT2","More POT Info");
  double pot2;
  double potTime2;
  bool hasPOT2;
  potTree2.Branch("time",&potTime2,"time/D");
  potTree2.Branch("POT",&pot2,"POT/D");

  TTree hornTree("Horn","Horn Current Info");
  double hornTime;
  double lineA, lineB, lineC, lineD;
  bool hasHorn;
  hornTree.Branch("time",&hornTime,"time/D");
  hornTree.Branch("lineA",&lineA,"lineA/D");
  hornTree.Branch("lineB",&lineB,"lineB/D");
  hornTree.Branch("lineC",&lineC,"lineC/D");
  hornTree.Branch("lineD",&lineD,"lineD/D");

  TTree mumonTree("NuMIMu","NuMI Muon Monitors");
  TTree mumonTreeAltTiming("NuMIMuAltTiming","NuMI Muon Monitors Alternate Timing");
  double mmTime,mmAltTimingTime;
  double mm1, mm2, mm3, mm1x, mm1y, mm2x, mm2y;
  double mm1cor, mm2cor, mm3cor, mm1gas,mm2gas,mm3gas;
  double tttgth;
  bool hasMM, hasMMAltTiming;
  mumonTree.Branch("time",&mmTime,"time/D");
  mumonTree.Branch("mm1",&mm1,"mm1/D");
  mumonTree.Branch("mm2",&mm2,"mm2/D");
  mumonTree.Branch("mm3",&mm3,"mm3/D");
  mumonTree.Branch("mm1x",&mm1x,"mm1x/D");
  mumonTree.Branch("mm1y",&mm1y,"mm1y/D");
  mumonTree.Branch("mm2x",&mm2x,"mm2x/D");
  mumonTree.Branch("mm2y",&mm2y,"mm2y/D");
  mumonTree.Branch("mm1cor",&mm1cor,"mm1cor/D");
  mumonTree.Branch("mm2cor",&mm2cor,"mm2cor/D");
  mumonTree.Branch("mm3cor",&mm3cor,"mm3cor/D");
  mumonTreeAltTiming.Branch("time",&mmAltTimingTime,"time/D");
  mumonTreeAltTiming.Branch("mm1gas",&mm1gas,"mm1gas/D");
  mumonTreeAltTiming.Branch("mm2gas",&mm2gas,"mm2gas/D");
  mumonTreeAltTiming.Branch("mm3gas",&mm3gas,"mm3gas/D");
  mumonTreeAltTiming.Branch("tttgth",&tttgth,"tttgth/D");



  TTree beamMonTree("BPM","NuMI Beam Position Monitors");
  double bpmTime;
  double gasMon2, onTargetX, onTargetY;
  bool hasBPM;
  beamMonTree.Branch("time",&bpmTime,"time/D");
  beamMonTree.Branch("gasMon2",&gasMon2,"gasMon2/D");
  beamMonTree.Branch("tgtX",&onTargetX,"tgtX/D");
  beamMonTree.Branch("tgtY",&onTargetY,"tgtY/D");

  TTree mumonArrayTree("NuMIMuArray","NuMI Muon Monitor Arrays");
  double mmArrayTime;
  double mma1ds[81] = {0};
  double mma1pd[81] = {0};
  double mma2ds[81] = {0};
  double mma2pd[81] = {0};
  bool hasMMArrays;
  mumonArrayTree.Branch("time",&mmArrayTime,"time/D");
  mumonArrayTree.Branch("mma1ds", mma1ds,"mma1ds[81]/D");
  mumonArrayTree.Branch("mma1pd", mma1pd,"mma1pd[81]/D");
  mumonArrayTree.Branch("mma2ds", mma1ds,"mma2ds[81]/D");
  mumonArrayTree.Branch("mma2pd", mma1pd,"mma2pd[81]/D");

//  presTree2.Branch("hasData",&hasPres2,"hasData/O");
  gROOT->cd();
  int entry=0;
  //Get a line
  while (!iss.eof())
  { 
    //clear variables: 
    pot = 0;
    potTime = 0;
    muTime =0;
    temp1=0;
    temp2=0;
    
    pres1=0;
    pres2=0;
    mmTime = 0;
    potTime2 = 0;
    hornTime = 0;
    p=0;
    pAr=0;
    pot2 = 0;
    lineA =0;
    lineB = 0;
    lineC = 0;
    lineD = 0;
    mm1 = 0;//Pedestal subtracted
    mm2 = 0;
    mm3 = 0;
    mm1x = 0;
    mm1y = 0;
    mm2x = 0;
    mm2y = 0;
    mm1cor =0;//Pedestal, calib, pres corrections
    mm2cor = 0;
    mm3cor = 0;


    mmAltTimingTime = 0;
    mm1gas = 0;//Gas pressures
    mm2gas = 0;
    mm3gas = 0;
    tttgth = 0;

    gasMon2 = 0;//Alcove 2 gas monitor (alpha source)
    onTargetX = 0;
    onTargetY = 0;   

    mmArrayTime = 0;
    fill(mma1ds, mma1ds+81, 0);
    fill(mma1pd, mma1pd+81, 0);
    fill(mma2ds, mma2ds+81, 0);
    fill(mma2pd, mma2pd+81, 0);

    hasPOT=false, hasMu=false, hasBPM=false;
    hasPOT2=false,hasHorn=false,hasMM=false,hasMMAltTiming=false;
    hasMMArrays=false;
    string line;
    getline(iss,line);
    if (line=="") break;
    istringstream lstr(line);
    //if (entry>55755) {cout <<"Entry: "<<entry<<endl;cout<<"Line: "<<line<<endl;}
 
/*
      string s("a\tb\tc\t\td\t");
  istringstream ss(s);
  string token;
  while (std::getline(ss,token,'\t')){
    cout << token <<endl;
*/
    //POT
    string token;
    std::getline(lstr,token,'\t');
    if (token.size()>0){
      potTime = t0 + atof(token.data());
      hasPOT = true;
    }
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      pot = atof(token.data());
// Temperature
    std::getline(lstr,token,'\t');
    if (token.size()>0){
      muTime =t0+ atof(token.data());
      hasMu = true;
    }
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      temp1 =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      temp2 =atof(token.data());
// Angle
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      pitch =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      yaw =atof(token.data());
//Pressure #1
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      pres1 =atof(token.data());

#ifndef USE2015
//Pressure #2
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      pres2 =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      p =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      pAr =atof(token.data());
#else
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');

#endif
    
//POT tree
    std::getline(lstr,token,'\t');
    if (token.size()>0){
      potTime2 = t0 + atof(token.data());
      hasPOT2 = true;
    }
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      pot2 = atof(token.data());

//Horn currents
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      hornTime =t0+ atof(token.data());
    std::getline(lstr,token,'\t');
    if (token.size()>0){
     // cout <<"Found P2 token: "<<token<<endl;
      lineA =atof(token.data());
      hasHorn = true;
    }
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      lineB =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      lineC =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      lineD =atof(token.data());
    
//NuMI MuMon
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mmTime =t0+ atof(token.data());
    // printf("mmTimeRel: %f\n", atof(token.data()));
    std::getline(lstr,token,'\t');
    if (token.size()>0){
     // cout <<"Found P2 token: "<<token<<endl;
      mm1 =atof(token.data());
    // printf("mm1: %f\n", atof(token.data()));
      hasMM = true;
    }
    std::getline(lstr,token,'\t');
    // printf("after mm1: %f\n", atof(token.data()));
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm2 =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm3 =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm1x =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm1y =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm2x =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm2y =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm1cor =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm2cor =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm3cor =atof(token.data());

    // start of mumonTreeAltTiming variables
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mmAltTimingTime =t0+ atof(token.data());
    std::getline(lstr,token,'\t');
    if (token.size()>0) {
      mm1gas =atof(token.data());
      if (mm1gas != 0) {
        hasMMAltTiming = true;
      }
    }
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm2gas =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      mm3gas =atof(token.data());
    
//Beam Position Monitor
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      bpmTime =t0+ atof(token.data());
    std::getline(lstr,token,'\t');
    if (token.size()>0){
      gasMon2 =atof(token.data());
      hasBPM = true;
    }
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      onTargetX =atof(token.data());
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      onTargetY =atof(token.data());




    //get TTTGTH from file
    std::getline(lstr,token,'\t');
    std::getline(lstr,token,'\t');
    if (token.size()>0)
      tttgth =atof(token.data());

// Arrays
    // std::getline(lstr,token,'\t');
    // if (token.size()>0) {
    //   if ( t0+atof(token.data()) != mmTime ){
    //     cout << "ERROR ERROR ERROR ERROR: First muon monitor array time doesn't match match mmTime" << endl;
    //     cout << "mmTime: " << mmTime << endl;
    //     cout << "array time: " << t0+atof(token.data()) << endl;
    //     printf("mmTime: %f \t\t array time: %f\n", mmTime, t0+atof(token.data()));
    //     printf("mmTimeRel: %f\n", atof(token.data()));
    //     return;
    //   }
    // } else {
    //   // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    //   for (int i = 0; i <= 32; i++) {
    //     getline(lstr,token,'\t');
    //     if (token.size()>0) mma1ds[i] = atof(token.data());
    //   }
    //   // the indices for this loop are the invalid array elements from the acnet numbering
    //   for (int i = 33; i <= 47; i++) {
    //     getline(lstr,token,'\t');
    //   }
    //         // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    //   for (int i = 33; i <= 80; i++) {
    //     getline(lstr,token,'\t');
    //     if (token.size()>0) mma1ds[i] = atof(token.data());
    //   }
    // }


    // std::getline(lstr,token,'\t');
    // if (token.size()>0) {
    //   if ( t0+atof(token.data()) != mmTime ){
    //     cout << "ERROR ERROR ERROR ERROR: Second muon monitor array time doesn't match match mmTime" << endl;
    //     return;
    //   }
    // } else {
    //   // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    //   for (int i = 0; i <= 32; i++) {
    //     getline(lstr,token,'\t');
    //     if (token.size()>0) mma1pd[i] = atof(token.data());
    //   }
    //   // the indices for this loop are the invalid array elements from the acnet numbering
    //   for (int i = 33; i <= 47; i++) {
    //     getline(lstr,token,'\t');
    //   }
    //         // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    //   for (int i = 33; i <= 80; i++) {
    //     getline(lstr,token,'\t');
    //     if (token.size()>0) mma1pd[i] = atof(token.data());
    //   }
    // }
    getline(lstr,token,'\t');
    if (token.size()>0) 
      mmArrayTime = t0 + atof(token.data());
    getline(lstr,token,'\t');
    if (token.size()>0){
      mma1ds[0] = atof(token.data());
      hasMMArrays = true;
      // printf("array data found: t0=%f\n",mmArrayTime);
    } //else {
      //printf("Error:  data file doesn't appear to contain arrays.\n");
      // return;
    //}

    // Now get the rest of the mma1 signal data (prev if statement took care of the zeroth entry in the array)
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 1; i <= 32; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma1ds[i] = atof(token.data());
    }
    // the indices for this loop are the invalid array elements from the acnet numbering
    for (int i = 33; i <= 47; i++) {
      getline(lstr,token,'\t');
    }
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 33; i <= 80; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma1ds[i] = atof(token.data());
    }
    // Now get the mma1 pedestal data
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 0; i <= 32; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma1pd[i] = atof(token.data());
    }
    // the indices for this loop are the invalid array elements from the acnet numbering
    for (int i = 33; i <= 47; i++) {
      getline(lstr,token,'\t');
    }
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 33; i <= 80; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma1pd[i] = atof(token.data());
    }


    // Now get the rest of the MMA2 signal data
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 0; i <= 32; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma2ds[i] = atof(token.data());
    }
    // the indices for this loop are the invalid array elements from the acnet numbering
    for (int i = 33; i <= 47; i++) {
      getline(lstr,token,'\t');
    }
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 33; i <= 80; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma2ds[i] = atof(token.data());
    }
    // Now get the mma1 pedestal data
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 0; i <= 32; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma2pd[i] = atof(token.data());
    }
    // the indices for this loop are the invalid array elements from the acnet numbering
    for (int i = 33; i <= 47; i++) {
      getline(lstr,token,'\t');
    }
    // these indices are from the 4th row of the array indexing libreoffice calc spreadsheet
    for (int i = 33; i <= 80; i++) {
      getline(lstr,token,'\t');
      if (token.size()>0) mma2pd[i] = atof(token.data());
    }


    if (hasPOT) potTree.Fill();
    if (hasMu) muTree.Fill();
    if (hasPOT2) potTree2.Fill();
    if (hasHorn) hornTree.Fill();
    if (hasMM) mumonTree.Fill();
    if (hasMMAltTiming) mumonTreeAltTiming.Fill();
    if (hasBPM) beamMonTree.Fill();
    if (hasMMArrays) mumonArrayTree.Fill();
    entry++;
  }
  f.cd();
  potTree.Write();
  muTree.Write();
  potTree2.Write();
  hornTree.Write();
  mumonTree.Write();
  mumonTreeAltTiming.Write();
  beamMonTree.Write();
  mumonArrayTree.Write();
  f.Close();

}

int main(int argc, const char* argv[])
{

  if (argc< 3){
    cout <<"Usage: ReadAcnetFile $INPUT $OUTPUT"<<endl;
    return -1;
  }
  FILE* f = fopen(argv[1],"r");
  if (!f){
    cout <<"Error: Input file "<<argv[1]<<" not opened for reading"<<endl;
    return -2;
  }
  string out;
  inf(f,out);
 
  istringstream iss(out);

  time_t t0;
  get_header(iss,t0);
  make_trees(iss,t0,argv[2]);
  return 0;
}
