#define DEBUG 1
#include "TROOT.h"
#include "TSystem.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TObject.h"
#include "TClonesArray.h"

#include <fstream>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <ctime>
#include <cassert>

#include <cmath>
#include <cstdlib>
#include <glob.h>
using namespace std;

static const int NSCOPE=10;

struct muon_data_str
{
  time_t time;
  int usec;
  int nchan;
  double start_t;
  double delta_t;
  vector< vector<float> > scope_data;
  vector<int> first_pt;
};

void get_timestamp(string line,muon_data_str& data);
void get_channel_info(string line,muon_data_str& data);
void build_channels(string line,muon_data_str& data);
void read_data(istringstream& in, muon_data_str& data);
muon_data_str read_file(const char* file);
void copy_scope_data(muon_data_str data,TClonesArray* arr);
void clear_scope_data(TClonesArray* arr);


int main(int argv,const char* argc[])
{

  //32-bit float

  int float_bit = sizeof(float)*CHAR_BIT;
  if (float_bit!=32)
  {
    cerr <<"Error: 32-bit float not found!"<<endl;
    cerr <<"Float is "<<float_bit<<" bits."<<endl;
    return 1;
  }

  if (argv<3){
    cerr<<"Usage: Please supply input and output filenames."<<endl;
    cerr<<"\tReadFile $OUTPUT $FILENAMES"<<endl;
    return 2;
  }

  //Set up tree

  //Note: TClonesArray may be better here
  TClonesArray* arr = new TClonesArray("TH1F",10,1);
  int usec,nchan;
  ULong64_t time;
  cout <<"Opening "<<argc[1]<<" for writing"<<endl;
  TFile f(argc[1],"RECREATE");
  f.cd();
  TTree t("MuonData","Muon Data");
  t.Branch("time",&time,"time/l");
  t.Branch("usec",&usec,"usec/I");
  t.Branch("nchan",&nchan,"nchan/I");
  t.Branch("data",&arr,512000,1);
  //Read data from file and copy to tree variables
  string arg2 = argc[2];
  bool useDir = false;
  vector<string> file_list;
  if (arg2=="--dir"){
    useDir = true;
    glob_t result;
    string path=string(argc[3])+"*.dat";
    cout <<"Checking path " << path << endl;
    glob( path.c_str(),GLOB_TILDE,NULL,&result);
    cout <<"Found "<<result.gl_pathc<<" files"<<endl;
    for (unsigned int i = 0; i<result.gl_pathc;i++)
      file_list.push_back(string(result.gl_pathv[i]));
    globfree(&result);
  }

  int Nfiles = useDir ? file_list.size() : argv;
  int istart = useDir ? 0 : 2;
  for (int i = istart; i<Nfiles; i++)
  {
    cout <<"Now reading "<< (useDir ? file_list[i] : argc[i])<<endl;

    muon_data_str data = read_file( (useDir ? file_list[i].c_str() : argc[i]));
    if (data.nchan==0){
      cerr << "Error: No channels found! File likely failed to open."<<endl;
      return 3;
    }
    nchan = data.nchan;
    //cout<<"Time: "<<data.time<<" "<<data.usec<<endl;
    time = data.time;
    usec = data.usec;
   
    copy_scope_data(data,arr);
    t.Fill();
    clear_scope_data(arr);
    
  }
 
  f.cd();
  t.Write();
  f.Close();
  return 0;
  
}


muon_data_str read_file(const char* file)
{

  ifstream f(file);
  if (!f){
    cout <<"Error: Input file " << file<<" not opened for reading"<<endl;
    return muon_data_str();
  }

  stringstream in;
  in << f.rdbuf();
  string out(in.str());
  f.close();
  string line1,line2,line3;
  getline(in,line1);
  getline(in,line2);
  getline(in,line3);

  muon_data_str data;
#ifdef DEBUG
  cout <<"Getting timestamp"<<endl;
#endif
  get_timestamp(line1,data);
#ifdef DEBUG
  cout <<"Getting channel info"<<endl;
#endif
  get_channel_info(line2,data);
#ifdef DEBUG
  cout <<"Building vectors"<<endl;
#endif
  build_channels(line3,data);
#ifdef DEBUG
  cout <<"Data: "<<endl;
  cout<<"T: "<<data.time<<" "<<data.usec<<endl;
  cout<<"Start/DeltaT "<<data.start_t<< " "<<data.delta_t<<endl;
  cout <<"NChan: "<<data.nchan<<endl;
#endif
  
  //Get Position
  streampos pos = in.tellg();
#ifdef DEBUG
  cout <<"Binary start position: "<<pos<<endl;
  cout <<"Closing stream"<<endl;
#endif
//  in.close();
#ifdef DEBUG
  cout <<"Reopening stream"<<endl;
#endif

  istringstream in2(out,ios::in | ios::binary);

#ifdef DEBUG
  cout <<"Returning to position"<<endl;
#endif
  in2.seekg(pos);
#ifdef DEBUG
  cout <<"Reading data"<<endl;
#endif
  read_data(in2,data);
#ifdef DEBUG
  cout <<"Final position: "<<in2.tellg()<<endl;
#endif
  in2.seekg(0,in2.end);
#ifdef DEBUG
  cout <<"End of file: "<<in2.tellg()<<endl;
#endif
//  in.close();

  return data;
//  return 0;
/*
  ofstream out0("test0.txt");
  out0<<"Data/F"<<endl;
  for (unsigned int i = 0 ; i < data.scope_data[0].size(); i++)
    out0<<data.scope_data[0][i]<<endl;
  out0.close();

  ofstream out1("test1.txt");
  out1<<"Data/F"<<endl;
  for (unsigned int i = 0 ; i < data.scope_data[1].size(); i++)
    out1<<data.scope_data[1][i]<<endl;
  out1.close();

  ofstream out2("test2.txt");
  out2<<"Data/F"<<endl;
  for (unsigned int i = 0 ; i < data.scope_data[2].size(); i++)
    out2<<data.scope_data[2][i]<<endl;
  out2.close();

  ofstream out3("test3.txt");
  out3<<"Data/F"<<endl;
  for (unsigned int i = 0 ; i < data.scope_data[3].size(); i++)
    out3<<data.scope_data[3][i]<<endl;
  out3.close();

  ofstream out4("test4.txt");
  out4<<"Data/F"<<endl;
  for (unsigned int i = 0 ; i < data.scope_data[4].size(); i++)
    out4<<data.scope_data[4][i]<<endl;
  out4.close();



  return 0;
  */
}

void
copy_scope_data(muon_data_str data,TClonesArray* arr)
{
  for (int i = 0; i < data.nchan; i++)
  {
    vector<float>& values = data.scope_data[i];
    int npt = (int)values.size();
    int fpt = data.first_pt[i];
    double t0 = (data.start_t + fpt * data.delta_t) * 1e6;
    double tf = (data.start_t + (fpt+npt) * data.delta_t)*1e6;
    TString name = TString::Format("ch%i",i);
    TString title = TString::Format("Ch. %i;t [#mus];Voltage [mV]",i);
    TH1F* h = new( (*arr)[i]) TH1F(name,title,npt,t0,tf);
    for (int j = 0 ; j < npt ; j++)
      h->SetBinContent(j+1,1000*values[j]);
    
  }



}

void
clear_scope_data(TClonesArray* arr)
{
  arr->Delete();
}

void get_timestamp(string line,muon_data_str& data)
{
//!!! WARNING: Currently assumes no underscores in pathname except to delimit the timestamp part of the filename !!!
  istringstream iss(line);
  string tmp;
  getline(iss,tmp,'_');  
#ifdef DEBUG
  cout <<tmp<<endl;
#endif
  getline(iss,tmp,'_');
#ifdef DEBUG
  cout <<tmp<<endl;
#endif
  istringstream iss2(tmp);
  iss2>>data.time;

  getline(iss,tmp,'_');
  tmp = tmp.erase(tmp.size()-4);
#ifdef DEBUG
  cout <<tmp<<endl;
#endif
  data.usec = atoi(tmp.c_str());

}

void
get_channel_info(string line, muon_data_str& data)
{
  string tmp;
  istringstream iss(line);
  iss >> tmp >> data.nchan >>tmp >> data.start_t>> tmp >> data.delta_t;
}

void
build_channels(string line,muon_data_str& data)
{
#ifdef DEBUG
  cout <<line<<endl;
#endif
  data.scope_data.clear();
  data.first_pt.clear();
  istringstream iss(line);
  string tmp;
  int fpt,npt,chan;
  for (int i = 0; i < data.nchan; i++)
  {
    iss >> tmp >> chan >> tmp >> fpt>>tmp >>npt;
    data.first_pt.push_back( fpt );
#ifdef DEBUG
    cout <<"Chan: "<<chan<<" Fpt "<<fpt<<" Npt "<<npt<<endl;
#endif
    data.scope_data.push_back( vector<float>(npt) );
  }
}


void
read_data(istringstream& in, muon_data_str& data)
{
  float tmp; 
// (1) Files are written back-to-back
// ** This seems to be the correct format **
  int nscope = (int)data.scope_data.size();
  for (unsigned int i = 0; i < data.scope_data.size(); i++)
  {
    //First element is useless: not actually part of array?
    in.read((char*)&tmp,sizeof(float));

    if ( (data.time > 1426788860L && data.time < 1427303870L)|| (data.time>1428032511L&&data.time<1428595416L)){//End time not yet exact
    //Channel off-by-one problem
      for (unsigned int j = 0; j < data.scope_data[(i+1)%nscope].size(); j++){
        in.read((char*)( &(data.scope_data[(i+1)%nscope][j]) ),sizeof(float));

      }
    }else{//Normal readout
      for (unsigned int j = 0; j < data.scope_data[i].size(); j++)
      {
      //Read a float
        in.read((char*)( &(data.scope_data[i][j]) ),sizeof(float));
      }
    }
  }

  return;
// (2) Data is interwoven ( ch1[0] ch2[0] ch3[0] ch4[0] ch5[0] ch1[1] ch2[1] ch3[1] ...)

  //Not done yet
}
