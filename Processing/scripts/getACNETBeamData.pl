#!/usr/bin/perl

# getACNETdata
# 
#
# Created by Geoffrey Mills on 3/26/15.
# Copyright 2015 __MyCompanyName__. All rights reserved.

use LWP;
use LWP::Simple;
use Getopt::Std;
use IO::Compress::Gzip qw(gzip $GzipError) ;

my %opts=();

getopts('d:m:y:',\%opts);


# print $opts{d} . $opts{m} . $opts{y} . "\n";

if($opts{d}) {
	#	print "Value of -d flag: " . $opts{d} . "\n";
	$day=$result = sprintf("%02d", $opts{d});
} else {
	$day="01";
}

if($opts{m}) {
	#	print "Value of -m flag: " . $opts{m} . "\n";
	$month=sprintf("%02d", $opts{m});
} else {
	$month="04";
}

if($opts{y}) {
	#	print "Value of -y flag: " . $opts{y} . "\n";
	$year=$opts{y};
} else {
	$year="2015";
}

# print "date is $year/$month/$day \n";

$dat="$month/$day/$year";
$tim="00:00:00";

# gzipped output file name:
#$output="beamACNETdata-$year$month$day.dat.gz";
#$output="hornACNETdata-$year$month$day.dat.gz";

my $dir="/usr/users/jplopez/data41/DUNE/data/acnet/$year/";
mkdir($dir) unless (-d $dir);
$dir="/usr/users/jplopez/data41/DUNE/data/acnet/$year/$month/";
mkdir($dir) unless (-d $dir);

$output="/usr/users/jplopez/data41/DUNE/data/acnet/$year/$month/muonACNETdata-$year$month$day.dat.gz";

my $zippy = new IO::Compress::Gzip $output 
	or die "gzip failed: $GzipError\n";

# convert date and time to "seconds-since-1970-..."
#Mac Version
#$t0=`date -jf "%m/%d/%Y %T" "$dat $tim" +"%s"`;
#Linux version
$t0=`date -d "$dat $tim" "+%s"`;
$t1=$t0+86400;

# what the data Logger wants is milli-seconds
$t0ms=$t0*1000;
$t1ms=$t1*1000;
#print "t0 is $t0ms ms \n";
#print "t1 is $t1ms ms \n";

# this command string was copied out of the dataLogger web-browser link-text window
# it returns table with time stamps separated by the following columns:
#
#
#
# E:TORTGT: 10^12
# E:NSLINA-D: kA
# E:MM#YAV: in
# E:MM#XAV: in
# E:MM#CNT
# E:MM#COR
# E:MGSMPD[105]
# E:HPTGT: mm
# E:VPTGT: mm
# E:TRTGTD: 10^12
# E:MM#GPR: torr
# E:MA2P01 the Cherenkov pressure in psi
# E:MA2P02: psia - vac. pump inlet
# E:MA2P03: torr
# E:MA2PAR: psig - rack argon
# E:MA2T01 the temperature on the Cherenkov detector (degrees Celsius)
# E:MA2T01 the temperature the Cherenkov detector (degrees Celsius)
# E:MA2X01 the Cherenkov pitch angle (%)
# E:MA2X02 the Cherenkov yaw angle (%)
#


## NSLINA: e,AE,e,770
## MM1INT: e,AE,e,1000
#POT
#$data=get("http://www-bd.fnal.gov/D44/dataLogger?t0=$t0ms&t1=$t1ms&how2plot=all&how_much=all&x=Time&y0=E:TRTGTD&y0_node=96&y0_color=black&Y0_min=auto&Y0_max=auto&Y0_event=e,AE,e,900&txt=true");
#Horn Current
$data=get("http://www-bd.fnal.gov/D44/dataLogger?t0=$t0ms&t1=$t1ms&how2plot=all&how_much=all&x=Time&y0=E:TORTGT&y0_node=96&Y0_event=null&y1=E:MA2T01&y1_node=96&Y1_event=null&y2=E:MA2T02&y2_node=96&Y2_event=null&y3=E:MA2X01&y3_node=96&Y3_event=null&y4=E:MA2X02&y4_node=96&Y4_event=null&y5=E:MA2P01&y5_node=96&Y5_event=null&y6=E:MA2P02&y6_node=96&Y6_event=null&y7=E:MA2P03&y7_node=96&Y7_event=null&y8=E:MA2PAR&y8_node=96&Y8_event=null&y9=E:TRTGTD&y9_node=96&Y9_event=null&y10=E:NSLINA&y10_node=96&Y10_event=null&y11=E:NSLINB&y11_node=96&Y11_event=null&y12=E:NSLINC&y12_node=96&Y12_event=null&y13=E:NSLIND&y13_node=96&Y13_event=null&y14=E:MM1CNT&y14_node=96&Y14_event=null&y15=E:MM2CNT&y15_node=96&Y15_event=null&y16=E:MM3CNT&y16_node=96&Y16_event=null&y17=E:MM1XAV&y17_node=96&Y17_event=null&y18=E:MM1YAV&y18_node=96&Y18_event=null&y19=E:MM2XAV&y19_node=96&Y19_event=null&y20=E:MM2YAV&y20_node=96&Y20_event=null&y21=E:MM1COR&y21_node=96&Y21_event=null&y22=E:MM2COR&y22_node=96&Y22_event=null&y23=E:MM3COR&y23_node=96&Y23_event=null&y24=E:MM1GPR&y24_node=96&Y24_event=null&y25=E:MM2GPR&y25_node=96&Y25_event=null&y26=E:MM3GPR&y26_node=96&Y26_event=null&y27=E:MGSMPD[105]&y27_node=96&Y27_event=null&y28=E:HPTGT&y28_node=96&Y28_event=null&y29=E:VPTGT&y29_node=96&Y29_event=null&txt=true");
#$data=get("http://www-bd.fnal.gov/D44/dataLogger?t0=$t0ms&t1=$t1ms&how2plot=all&how_much=all&x=Time&y0=E:MM1INT&y0_node=96&Y0_event=null&y1=E:MM2INT&y1_node=96&Y1_event=null&y2=E:MM3INT&y2_node=96&Y2_event=null&txt=true");

#$data=get("http://www-bd.fnal.gov/D44/dataLogger?t0=$t0ms&t1=$t1ms&how2plot=all&how_much=all&x=Time&y1=E:NSLINA&y1_node=96&y1_color=green&Y1_min=auto&Y1_max=auto&Y1_event=e,AE,e,770&y2=E:NSLINB&y2_node=96&y2_color=blue&Y2_min=auto&Y2_max=auto&Y2_event=e,AE,e,770&y3=E:NSLINC&y3_node=96&y3_color=red&Y3_min=auto&Y3_max=auto&Y3_event=e,AE,e,770&y4=E:NSLIND&y4_node=96&y4_color=brown&Y4_min=auto&Y4_max=auto&Y4_event=e,AE,e,770&txt=true");
#&y5=E:MM1INT&y5_node=138&y5_color=brown&Y5_min=auto&Y5_max=auto&Y5_event=e,AE,e,900&y6=E:MM2INT&y6_node=138&y6_color=brown&Y6_min=auto&Y6_max=auto&Y6_event=e,AE,e,900&y7=E:MM3INT&y7_node=138&y7_color=brown&Y7_min=auto&Y7_max=auto&Y7_event=e,AE,e,900&txt=true");
print $zippy "T0 $t0ms T1 $t1ms \n";
print $zippy "$data\n";
$zippy->close();

