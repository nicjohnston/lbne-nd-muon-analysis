/** Test of using c++11 map to convert a channel name to a variable

Author: Nicholas Johnston
Date: May 2019
*/

#include "TFile.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TTree.h"
#include "TH1.h"
#include "TMath.h"
#include <cstdio>
#include <cstring>
#include <cassert>
#include <string>
#include <sstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>

using namespace std;

int lineIdent(const char* filename) {

	vector<double> tortgt;
	vector<double> nslina;
	vector<double> mma2dsfull;


	vector<double> v{13,14,15};

	map<string,vector<double>*> varmap;
	varmap["vec1"] = &v;
	cout << "Size: " << varmap["vec1"]->size() << endl;
	cout << varmap["vec1"]->at(0) << endl;
	varmap["vec1"]->at(0) = 21;
	cout << varmap["vec1"]->at(0) << endl;
	return 0;
}

int main(int argc, const char* argv[]) {
	lineIdent(argv[1]);
	return 0;
}
