/** Test of using c++11 map to convert a channel name to a variable

Author: Nicholas Johnston
Date: May 2019
*/

#include "TFile.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TTree.h"
#include "TH1.h"
#include "TMath.h"
#include <cstdio>
#include <cstring>
#include <cassert>
#include <string>
#include <sstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>

using namespace std;

int main(int argc, const char* argv[]) {
	double var1 = 1.243;
	double var2 = 3.421;
	double arr1[2] = { 2.4, 4.2 };
	vector<double> v{13,14,15};

//	map<string,double*> varmap;
	map<string,vector<double>*> varmap;
	//varmap.insert(make_pair("var1",1));
//	varmap["var1"] = &var1;
//	varmap["var2"] = &var2;
//	varmap["arr1"] = arr1;
	varmap["vec1"] = &v;
	cout << "Size: " << varmap["vec1"]->size() << endl;
	cout << varmap["vec1"]->at(0) << endl;
	varmap["vec1"]->at(0) = 21;
	//cout << varmap[argv[1]] << endl;

	map<string,vector<double>*>::iterator i = varmap.find(argv[1]);
	if (i == varmap.end()) {
		/* Not found */
		cout << "not found" << endl;
	} else {
		/* Found, i->first is f, i->second is ++-- */
		cout << "found" << endl;
		//cout << varmap[argv[1]] << endl;
		cout << i->second->at(0) << endl;
	}
	//varmap.insert(make_pair("var2",&var2));
//	map<string,double*>::iterator it = varmap.begin();
//	while (it != varmap.end()) {
//		cout << it->first << " :: " << it->second << " :: " << *it->second << endl;
//		it++;
//	}
	//double * variable = varmap["arr1"];
	//cout << sizeof(variable)/sizeof(variable[1]) << endl;
	return 0;
}
